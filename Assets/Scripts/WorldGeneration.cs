﻿using UnityEngine;
using System.IO;
using System.Text;
using System;
using System.Linq;


public class WorldGeneration : MonoBehaviour
{
	private int[,] caveMap;

	public BlockData BD;
	public BlockBehaviour BB;
	private FileInfo[] terrainList; //List of terrain files


	private byte[] ascii; //Ascii value for each terrain

	private int id;
	private int terrainCounter;
	private bool taken;

	private int minWidth = 0;
	private int minHeight = 0;
	private int maxWidth = 0;
    private int maxHeight = 75;

    public WorldGenerationData WGD;
	public GridIDHandler GID;

	PerlinNoise noise;

	public void GenerateTerrain()
    {
		
        noise = new PerlinNoise(WGD.key); //Generate perlin noise

        Debug.Log(WGD.key);

		//Check if models has been properly loaded before generating the world
		if (GID.blocks == null) {
			Debug.Log ("Somethings wrong! Empty array");}
		

		Debug.Log(WGD.terrains.Length);

		//If terrains haven't been loaded before. Go through all terrains to later use them
		if(WGD.terrains.Length < 1)
		{
			Debug.Log ("No Terrains Stored! Updating new ones");
			GetTerrains(); //Load all terrains

			int count = 0; //Counting all usable terrains 
			foreach(string s in WGD.terrains)
			{
				if (s == null) //Skip empty files
				{
					count++;
					continue;
				}
				Debug.Log (s);

				//Get sum of each Character in Ascii, from each terrain name
				int value = 0;
				ascii = Encoding.ASCII.GetBytes(s);
				foreach(byte b in ascii)
				{
					value += b;
				}

				WGD.terrainOrder[count] = value;
				WGD.terrainValues[count] = value;

				Debug.Log (value);
				count++;
			}
			Array.Sort (WGD.terrainValues); //Sort the Ascii values to later know which order to place the terrains
		}

		maxWidth = 200 * WGD.terrains.Length; //For each terrain, the world gets wider

		GenerateCaves ();//Get a Vector 2 to know where to place blocks and where to not, when generating the world.

		//Placing blocks with perlin noise
		for(int x = minWidth; x < maxWidth; x++) // Generate Width of world from left to right
		{
			//Change which terrain to use
			if (x % (maxWidth/WGD.terrains.Length) == 0 && x != 0) 
			{
				Debug.Log ("Changing terrain at " + x);
				terrainCounter++;
			}
			//get the height to Instantiate blocks to
			int columnHeight = 10 + noise.getNoise (x - minWidth, maxHeight - minHeight - 10);
			
			for (int y = minHeight; y < minHeight + columnHeight; y++) // Generate Height of world down and up
			{
				for(int z = 0; z < 7 ;z++) //Generate layers in z axis
				{
					//If there has been a change (from old saves) skip this block
					foreach (string data in GID.blocks) 
					{
						if (data.Contains (x + ":" + y + ":" + z + ":"))  
						{
							taken = true;
							break;
						}
					}
					//If a block has been replaced or there is a cave, continue without placing a block
					if (taken || (caveMap[x,y] == 0 && columnHeight - 30 > y)) 
					{
						taken = false;
						continue;
					}
					//Get the type of block to place from the active terrain
					id = GetBlock (y, columnHeight, minHeight, ChooseTerrain ());
					//Place block
					Vector3 pos = new Vector3(x,y,z);
					BB.CreateBlock(id, pos, null);
				}

			}
		}
		Debug.Log("World Ready!" + maxHeight + " - max min - " + minHeight);
	}

	private int ChooseTerrain()//Returns the "id" value of active terrain
	{
		
		//Return index of terrainOrder where its value corresponds with terrainValue[terrainCounter]

		return Array.FindIndex(WGD.terrainOrder, ActiveTerrain);
	}

	private bool ActiveTerrain(int i)
	{
		
		if(i == WGD.terrainValues[terrainCounter])
		{
			return true;
		}
		return false;
	}

	private void GetTerrains()
	{
		string directory = Path.Combine(Application.dataPath, "Terrains");

		//Get a list of terrains
		DirectoryInfo dir = new DirectoryInfo(directory);
		terrainList = dir.GetFiles("*.txt"); //Use all files that ends with .txt (Extension)
		WGD.terrains = new string[terrainList.Length];
		WGD.terrainConfig =  new string[terrainList.Length];
		WGD.terrainOrder = new int[terrainList.Length];
		WGD.terrainValues = new int[terrainList.Length];
		//for every file, create terrain
		int count = 0;
		foreach (FileInfo f in terrainList)
		{
			string data = File.ReadAllText(Path.Combine(directory, f.Name));

			WGD.terrainConfig[count] = data;//Looks like ID:columnHeight|ID:columnHeight|ID:columnHeight...
			WGD.terrains[count] = f.Name;
			count++;
		}
	}

	private int GetBlock(int atHeight, int topHeight, int minHeight, int terrain) //Return which block to generate
	{

		string[] data = (WGD.terrainConfig [terrain]).Split ('|');

		foreach(string i in data)
		{
			if(i == "")
			{
				continue;
			}
			string[] tempArray = i.Split (':');
			if (tempArray [1] == "Else") //If "Else" is called, place the block where there is undeclaired values in the terrains config
			{
				return int.Parse(tempArray [0]);
			} else if (atHeight > minHeight + topHeight - int.Parse (tempArray [1]))  //Find the block to place after the current height generating the world at
			{
				return int.Parse(tempArray [0]);
			}

		} 

		return 1; //If there isn't anything configured, place id 1 (Stone by default)
	}


	public void InstantiateData(int[] data) //Instantiate the model
	{
		id = data [3]; 
		//avoid placing air (id 0)
		if (data[3] != 0) {
			BB.CreateBlock (id, new Vector3 (data [0], data [1], data [2]), null);
		}
	}

	public void GenerateCaves()
	{
		// Cave Generation Based on : https://unity3d.com/learn/tutorials/projects/procedural-cave-generation-tutorial/cellular-automata?playlist=17153 by Sebastian Lague

		caveMap = new int[maxWidth,maxHeight]; //Create vector 2 array to store either "1" or "0" for "true" or "false" 

		RandomFillMap(); //Set true/false values by noise

		//Shape Caves
		for (int i = 0; i < 5; i ++) 
		{
			SmoothMap();
		}
	}

	void RandomFillMap() 
	{
		System.Random caveNoise = new System.Random(WGD.key); //get noise used to generate caves

		//Go through all possible coordinates in the world
		for (int x = minWidth; x < maxWidth; x ++) {
			for (int y = minHeight; y < maxHeight; y ++) 
			{
				
				if (x == 0 || x == maxWidth || y == 0 || y == maxHeight) //Avoid placing a cave where it's too far to the edge
				{
					caveMap[x,y] = 1;
					continue;
				}

				caveMap[x,y] = (caveNoise.Next(0, 100) < 50)? 1: 0; //Place caves by noise
			}
		}
	}

	void SmoothMap()
	{
		for (int x = minWidth; x < maxWidth; x++) {
			for (int y = minHeight; y < maxHeight; y++) 
			{
				int neighbourWallBlock = GetSurroundingWallCount (x,y);

				if(neighbourWallBlock > 4)
				{
					caveMap [x, y] = 1;
				}else if(neighbourWallBlock < 4)
				{
					caveMap[x, y] = 0;
				}
			}
		}
	}



	int GetSurroundingWallCount(int gridX, int gridY)
	{
		int wallCount = 0;
		for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX ++) {
			for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY ++) {
				if (neighbourX >= 0 && neighbourX < maxWidth && neighbourY >= 0 && neighbourY < maxHeight) {
					if (neighbourX != gridX || neighbourY != gridY) {
						wallCount += caveMap[neighbourX,neighbourY];
					}
				}
				else {
					wallCount ++;
				}
			}
		}
		return wallCount;
	}
}
