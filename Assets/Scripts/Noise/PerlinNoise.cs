﻿using UnityEngine;

public class PerlinNoise
{
	long seed;

	public PerlinNoise (long seed)
	{
		this.seed = seed;
	}

	private int random(int x, int range)
	{
		return (int)((x + seed) ^ 5) % range;
	}

	public int getNoise(int x, int range)
	{
		int chunkSize = 16;

		float noise = 0;

		range /= 2;

		while (chunkSize > 0)
		{

			int chunkIndex = x / chunkSize;

			float progress = (x % chunkSize) / (1f * chunkSize);

			float lRandom = random (chunkIndex, range);
			float rRandom = random (chunkIndex + 1, range);

			noise += (1 - progress) * lRandom + progress * rRandom;
			chunkSize /= 2;
			range /= 2;
			range = Mathf.Max (1, range);
		}

		return ((int)Mathf.Round(noise));
	}
}

