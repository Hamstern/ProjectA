﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class SaveManager : MonoBehaviour
{

	public WorldGeneration WG;
	public WorldGenerationData WGD;
	public GridIDHandler GID;

	private string path;
	private string fileName;
	private string jsonString;
	private string gridData;
	string worldName;

    public void SaveAll()
    {
		path = Path.Combine(Application.persistentDataPath, Path.Combine("Saves", WGD.worldName));
		Debug.Log ("Saving accomplished data");
        //Save everything
		SaveWorldData(WGD.worldName);
		SaveGridID(WGD.worldName);
		Debug.Log ("Done!");
    }
	public void LoadAll()
    {
		path = Path.Combine(Application.persistentDataPath, Path.Combine("Saves", WGD.worldName));
		Debug.Log ("Getting previous data");
        //Load everything there is to load
		LoadWorldData(WGD.worldName);
		LoadGridID (WGD.worldName);
		Debug.Log ("Done!");
    }
    
	void SaveWorldData(string worldName) //Save information about the world
    {
		
		fileName = worldName + ".txt";

        //Save data to a string to later print the string to a file
        jsonString = JsonUtility.ToJson(WGD);
		Debug.Log (Path.Combine(path, fileName));
		File.WriteAllText(Path.Combine(path, fileName), jsonString);
		Debug.Log ("World Data Saved!");
    }
    
    
	void LoadWorldData(string worldName) //Load information about the world
    {
		fileName = worldName + ".txt";
        //Read a file and put the information to a string
		jsonString = File.ReadAllText(Path.Combine(path, fileName));
        //Read the string and put the information to the Game object
		JsonUtility.FromJsonOverwrite(jsonString, WGD);
		Debug.Log ("World Data Loaded!");
    }

    void SaveGridID(string worldName) //Save changes made in the world
	{
		fileName = "GridID.txt"; //The filename to store in

        //Save data to a string to later print the string to a file
		foreach (string obj in GID.blocks) 
		{
			gridData += obj + "|";
			Debug.Log (obj);
		}
		//put stuff in file
		File.WriteAllText(Path.Combine(path, fileName), gridData); //Looks like X:Y:Z:ID|X:Y:Z:ID|X...
		gridData = null;

		Debug.Log ("World Changes Saved!");
    }

    void LoadGridID(string worldName) //Load changes made in a world
    {
		fileName = "GridID.txt";
        //Read a file and put the information to a string
		jsonString = File.ReadAllText(Path.Combine(path, fileName));
        //Read the string and put the information to the Game object

		//JsonUtility.FromJsonOverwrite(jsonString, GID);

		string[] gridData = jsonString.Split('|');//Looks like X:Y:Z:ID\nX:Y:Z:ID\nX... (\n is "a new array/line")
		int[] seperator;
		foreach(string data in gridData)
		{
			if (data == "" || data == null) {
				continue;
			} else {
				seperator = Array.ConvertAll<string, int> (data.Split (':'), int.Parse);
				WG.InstantiateData (seperator);
				GID.UpdateData (seperator);
				seperator = null;
			}

		}
		Debug.Log ("World Changes Loaded!");

    }

}
