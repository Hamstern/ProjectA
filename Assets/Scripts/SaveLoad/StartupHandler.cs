﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class StartupHandler : MonoBehaviour
{
	public BlockData BD;
    public SaveManager SM;
    public WorldGeneration WG;
    public WorldGenerationData WGD;
    public PlayerSpawnHandler PSH;

    // Use this for initialization
    void Start()
    {
        Debug.Log("Hello World!");
		BD.LoadConfig ();
        //Get the world name and the key the player has set
        WGD.key = PlayerPrefs.GetInt("worldKey");
        WGD.worldName = PlayerPrefs.GetString("worldName");
		string path = Path.Combine(Application.persistentDataPath, Path.Combine("Saves", WGD.worldName));
		CreateDirectory (path);
		LoadWorld(path);

    }

	private void CreateDirectory(string path)
	{
		//Create the directory to save the files if it doesn't already exist
		try
		{
			Debug.Log(path);
			if(!Directory.Exists(path))
			{
				Directory.CreateDirectory (path);
				Debug.Log ("Created new directory");
			}else{

				Debug.Log("Path exists!");
			}
		}catch(IOException e){
			Debug.Log (e.Message);
		}
	}

    private void LoadWorld(string path)
	{

        //Look if world already exists
		if (File.Exists(Path.Combine(path , WGD.worldName + ".txt")))
        {
             //What to do if the world already exists
             SM.LoadAll(); //Load World Information
        }else{
             //If it is a new world
			Debug.Log("New world in progress!");
        }
        //Start creating the world
        WG.GenerateTerrain(); //Generate world by perlin noise

        PSH.SpawnPlayer(); //Instantiate player

		SM.SaveAll(); //Save World / Overwrite
    }



	
}
