﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridIDHandler : MonoBehaviour
{

	public ArrayList blocks = new ArrayList();
	private bool replaced = false;

    /*
        When something happens in the world (block placed or destroyed) the gameobject that's adjusted, have to give information to this script
        The purpose is to be able to load everything on startup but the changed places in the world, that will be placed afterwards relying on this information. (It will ignore everything placed naturally on world creation)
        Simply put, it stores every changed part in the world.
    */
	public void AddWorldChange(int ID, Vector3 pos) //Is to be called when something in the world has changed (block placed or destroyed)
    {
		replaced = false;
		blocks.Remove(FindReplaceBlock (ID, pos));
		Debug.Log ("Removed");
		if (!replaced) {
			Debug.Log ("replacing");
			blocks.Add (pos.x.ToString () + ":" + pos.y.ToString () + ":" + pos.z.ToString () + ":" + ID.ToString ());
		}

		//Sucessfully Stored Change!
	}

	public void UpdateData(int[] seperator)
	{		
		//Update the array with information about the world (Used when loading old saves)
		blocks.Add(seperator[0].ToString() + ":" + seperator[1].ToString() + ":" + seperator[2].ToString() + ":" + seperator[3].ToString());
	}
		
	public object FindReplaceBlock(int ID, Vector3 pos)
	{
		//Add coordinate and id to a list. 0 (air) if destorying a object
		foreach (string data in blocks) 
		{
			//Replaces data where something is removed or placed
			if (data.Contains (pos.x + ":" + pos.y + ":" + pos.z + ":")) {
				Debug.Log ("replacing " + data + " with " + pos.x.ToString () + ":" + pos.y.ToString () + ":" + pos.z.ToString () + ":" + ID.ToString ());
				return data;
			}
		}
		return null;
	}

}
