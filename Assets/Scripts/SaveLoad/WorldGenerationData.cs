﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerationData : MonoBehaviour
{

    public string worldName;
    public int key;
    public bool generated;
	public string[] terrains;
	public string[] terrainConfig; // String with terrain information
	public int[] terrainOrder;//TerrainValues placed where the terrain is
	public int[] terrainValues; //Values of each terrain


}

    
