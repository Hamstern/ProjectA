﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBehaviour : MonoBehaviour //Better name, Block handler
{
	
	public BlockData BD;

	public void CreateBlock(int id, Vector3 pos, Transform parent)
	{
		GameObject tempObj = BD.objectStructure;

		if (tempObj.GetComponent<MeshFilter>() == null) 
		{
			tempObj.AddComponent<MeshFilter> ();
		}
		if(tempObj.GetComponent<MeshRenderer>() == null)
		{
			tempObj.AddComponent<MeshRenderer> ();
		}
		tempObj.GetComponent<MeshFilter> ().sharedMesh = BD.GetMesh(id);
		tempObj.GetComponent<MeshRenderer> ().material = BD.GetMaterial(id);

			
		Construct (pos, tempObj, parent);
	}

	public void Construct(Vector3 pos, GameObject model, Transform parent)
	{
		Instantiate (model , pos, Quaternion.identity, parent);
	}


}