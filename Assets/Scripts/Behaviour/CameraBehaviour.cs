﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour 
{	
	Vector3 speed;
	Vector3 rotation;

	public Transform target;

	private float time = 0.2f;
	private float sensitivity = 5;
	private float distance = 20;
	private float horizontalA;
	private float verticalA;

	private int minY = -20;
	private int maxY = 20;

	void LateUpdate () 
	{
		Rotate ();
		Zoom ();
	}

	private void Rotate()
	{
		if (Input.GetMouseButton (2)) { //If Middle Mouse Button is down
			horizontalA += Input.GetAxis ("Mouse X") * sensitivity;
			verticalA -= Input.GetAxis ("Mouse Y") * sensitivity;
			verticalA = Mathf.Clamp (verticalA, minY, maxY);
		}

		rotation = Vector3.SmoothDamp (rotation, new Vector3 (verticalA, horizontalA), ref speed, time);
		transform.eulerAngles = rotation;

		transform.position = target.position - transform.forward * distance;

	}

	private void Zoom()
	{
		if(Input.GetKey(KeyCode.Q))
		{
			distance -= Mathf.FloorToInt (Input.mouseScrollDelta.y);

			if(distance > 30)
			{
				distance = 30;
			}else if(distance < 10)
			{
				distance = 10;
			}
		}
		if(Input.GetKey(KeyCode.LeftControl))
		{
			distance = 400;
		}
	}

}
