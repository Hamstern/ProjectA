﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviourHumanoid : MonoBehaviour 
{
	public BlockData BD;
	public int selectedBlock;
	private int maxID;

	void Start()
	{
		maxID = BD.models.Length - 1;
		selectedBlock = 0;
	}

	void Update()
	{
		selectedBlock += Mathf.FloorToInt(Input.mouseScrollDelta.y);
		if (selectedBlock > maxID) 
		{
			selectedBlock = 1;
		} else if (selectedBlock < 1) 
		{
			selectedBlock = maxID;
		}
	}
}
