﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour 
{
	private bool getSmaller;
	private float smallerSpeed;
	private float biggerSpeed;

	void Start()
	{
		getSmaller = true;
		smallerSpeed = 0.005f;
		biggerSpeed = 0.01f;
	}

	// Update is called once per frame
	void Update () 
	{
		SetSize ();
	}
	private void SetSize()
	{
		if(gameObject.transform.localScale.x > 8.1f || gameObject.transform.localScale.x < 7.5f)
		{
			getSmaller = !getSmaller;
		}
		if (getSmaller) 
		{
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - smallerSpeed, gameObject.transform.localScale.y - smallerSpeed, gameObject.transform.localScale.z - smallerSpeed);
		} else 
		{
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + biggerSpeed, gameObject.transform.localScale.y + biggerSpeed, gameObject.transform.localScale.z + biggerSpeed);
		}
	}

}
