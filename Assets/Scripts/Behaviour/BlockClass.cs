﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockClass : MonoBehaviour 
{

	public GridIDHandler GIDH;
	public BlockData BD;
	public BlockBehaviour BB;
	private int block;
	public PlayerBehaviourHumanoid PBH;

	private int ID;

	public bool dispose;
	private float size;
	private bool getSmaller;


	void Dispose()
	{
		if(dispose)
		{
			size -= 0.05f;

			SetSize ();

			if (size < 0f) 
			{
				Destroy(gameObject);
			}
		}
	}

	private void SetSize()
	{
		gameObject.transform.localScale = new Vector3(size , size, size);
	}


	public void CreateBlock()
	{

		//Is called, when a block is placed etc, not on world generation
		//See the surface the mouse is pointing to place a block there
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		if(Physics.Raycast(ray, out hit))
		{
			ID = PBH.selectedBlock; //Get the block id from user

				GIDH.AddWorldChange(ID, transform.position + hit.normal); //List the block so it can be saved later
				BB.CreateBlock (ID, transform.position + hit.normal, null);
		}

	}

	public void DestroyBlock()
	{
		GIDH.AddWorldChange(0 , transform.position); //List the block as air
	}

	private void OnMouseOver()
	{
		if(!dispose)
		{
			if(Input.GetKeyDown(KeyCode.Mouse0)) //On left click
			{ 
				size = 1f;
				dispose = true;
				InvokeRepeating ("Dispose", 0f, Time.deltaTime);
				DestroyBlock ();
			}

			if(Input.GetKeyDown(KeyCode.Mouse1)) //On right click
			{
				CreateBlock ();
			}
		}

	}
		

}
