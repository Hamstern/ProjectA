﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	Ray ray;
	RaycastHit hit;
	GameObject grapplingHook;

    private CharacterController controller;
    private Vector2 moveVector;
	private LineRenderer lr;
	private Color grapplingHookColor = Color.gray;

    public float verticalVelocity;
    public float gravity;
    public float jumpForce = 8f;
	public float walkForce = 7f;
    public float horizontalForce;
	public float grapplingForce = 7f;
	public float windPressure = 0.5f;
	public float verticalGrapplingForce;
	public bool grapplingActive;
    public bool chargedJump;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        gravity = 9f;
    }

    private void Update()
    {
		//Edit the grappling hook
		if(grapplingActive)
		{
			EditHook (transform.position, hit.collider.transform.position);
			chargedJump = true;
		}

		if (controller.velocity.x == 0)
		{
			horizontalForce = 0f;
		}
		if(controller.velocity.y == 0)
		{
			verticalVelocity = 0f;
		}
        //Get vertical velocity
        if (controller.isGrounded)
		{
            chargedJump = true;
            verticalVelocity = -gravity; //Default vertical velocity
            if (Input.GetKeyDown(KeyCode.Space))
            {
                verticalVelocity = jumpForce; //Jump
            }
			if(Input.GetAxis("Horizontal") == 0)
			{
				horizontalForce = 0;	
			}

        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime; //Fall
        }

		//Walk or Grappling hook
		if (Input.GetKeyDown (KeyCode.E) && !grapplingActive) //Grappling hook
		{
			GrapplingHook ();
		} else if(Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.D)) // Walking
		{
			if ((controller.velocity.x > walkForce && Input.GetAxis ("Horizontal") > 0) || (controller.velocity.x < -walkForce && Input.GetAxis ("Horizontal") < 0)) //If the horizontal speed is greater than the walkspeed in said direction
			{
				//If horizontal speed is greater than walking speed

			} else {
				//If horizontal speed is not greater than walking speed
				horizontalForce += Input.GetAxis ("Horizontal") * walkForce * Time.deltaTime;
			}
		}

        //Add the horizontal and vertical velocity to movement
        moveVector = Vector2.zero;

		moveVector.x = horizontalForce;
		moveVector.y = verticalVelocity;

        //Move the player
        controller.Move(moveVector * Time.deltaTime); //Move

        
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //On Collision with wall wall
        if (!controller.isGrounded && hit.normal.y < 0.1f)
        {
            
            //If Space is pressed, do Cliff Jump
            if (Input.GetKeyDown(KeyCode.Space) && chargedJump == true)
            {
                verticalVelocity = jumpForce; //Cliff Jump
                chargedJump = false;
            }
        }
    }

    public void ToSpawn()
    {
        transform.position = new Vector3(0, 15, 0);
    }

	private void GrapplingHook()
	{
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out hit))
			{
				//Set forces depending on the hit colliders position
				if (hit.collider.name.Contains ("(Clone)"))
				{
					Debug.Log ("Called!!!");
				if (hit.collider.transform.position.y - transform.position.y < 0) // Grappling y Force
				{
					verticalVelocity -= grapplingForce;
				} else {
					verticalVelocity += grapplingForce;
				}
				if (hit.collider.transform.position.x - transform.position.x < 0) // Grappling x Force 
				{
					horizontalForce -= grapplingForce;
				} else {
					horizontalForce += grapplingForce;
				}
				}
				//Draw and destroy hook
				DrawHook (transform.position, hit.collider.transform.position);
				Invoke ("DestroyHook", 1f);
				grapplingActive = true;
			}

	}


	void DrawHook(Vector3 start, Vector3 end)
	{
		grapplingHook = new GameObject();
		grapplingHook.transform.position = start;
		grapplingHook.AddComponent<LineRenderer>();
		lr = grapplingHook.GetComponent<LineRenderer>();
		lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
		lr.startColor = lr.endColor = grapplingHookColor;

		lr.startWidth = 0.1f;
		lr.endWidth =0.1f;
		lr.SetPosition(0, start);
		lr.SetPosition(1, end);
	}
	void EditHook(Vector3 start, Vector3 end)
	{
		lr.SetPosition(0, start);
		lr.SetPosition(1, end);
	}

	void DestroyHook()
	{
		grapplingActive = false;
		GameObject.Destroy(grapplingHook);
	}
		
}