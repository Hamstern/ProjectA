﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class SpectateBlockAI : MonoBehaviour 
{
	public Dropdown meshD;

	public Dropdown materialD;
	public InputField URLIF;
	public InputField redIF;
	public InputField greenIF;
	public InputField blueIF;
	public InputField oXIF;
	public InputField oYIF;
	public bool isURL;


	private float rotSpeed;
	private string dir; //Find Meshes and Materials

	private MeshRenderer MR;

	public string imgURL;

	void Start()
	{
		//Position, Rotation, Animation

		gameObject.transform.localPosition = new Vector3 (30f, 180f, -490f);
		rotSpeed = 25f;
		MR = transform.GetComponent<MeshRenderer> ();
		MR.material.shader = Shader.Find("Diffuse");

	}

	void Update()
	{
		transform.Rotate (Vector3.up, rotSpeed * Time.deltaTime); //Rotation
	}

	public void SetMesh()
	{
		string model = meshD.options[meshD.value].text;
		dir = Path.Combine ("Meshes", model);

		transform.GetComponent<MeshFilter>().sharedMesh = ((GameObject)Resources.Load (dir)).GetComponent<MeshFilter> ().sharedMesh;
		Debug.Log ("Setting model to " + model);
		if (model == "Cylinder") {
			transform.localScale = new Vector3 (15f, 7.5f, 15f);
		} else {
			transform.localScale = new Vector3 (15f, 15f, 15f);
		}
	}

	public void SetMaterial()
	{
		
		imgURL = materialD.options [materialD.value].text;
		string material = imgURL;
		dir = Path.Combine(Application.dataPath, Path.Combine("Images", material));

		Debug.Log (dir);
		Texture2D tempMat = Resources.Load (dir, typeof(Texture2D)) as Texture2D;
		Debug.Log ("Setting texture to " + material);
		MR.material.mainTexture = tempMat;
		isURL = false;

	}

	public void SetMaterialURL()
	{
		imgURL = URLIF.text;
		WWW link = new WWW (imgURL);
		while(!link.isDone)
		{
		}
		Debug.Log ("Setting texture to " + link.url);
		MR.material.mainTexture = link.texture;
		isURL = true;
	}

	public void SetRGB()
	{
		byte red;
		byte green;
		byte blue;
		byte alpha = 0;

		if (redIF.text == "") {
			red = 0;
		} else {
			
			red = byte.Parse (redIF.text);
		}
		if (greenIF.text == "") {
			green = 0;
		} else {

			green = byte.Parse (greenIF.text);
		}
		if (blueIF.text == "") {
			blue = 0;
		} else {

			blue = byte.Parse (blueIF.text);
		}

		MR.material.color = new Color32(red,green,blue,alpha);
	}

	public void SetOffset()
	{
		float oX;
		float oY;
		if (oXIF.text == "") {
			oX = 0;
		} else {

			oX = float.Parse (oXIF.text);
		}
		if (oYIF.text == "") {
			oY = 0;
		} else {
			oY = float.Parse (oYIF.text);
		}
		MR.material.mainTextureOffset = new Vector2(oX, oY);
	}
}
