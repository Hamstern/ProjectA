﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnCreateBlock : MonoBehaviour {

	public void Show()
	{
		gameObject.SetActive (true);
	}
	public void Hide()
	{
		gameObject.SetActive (false);
	}

	public void ReverseVisibility()
	{
		switch(gameObject.activeSelf)
		{
		case true:
			gameObject.SetActive (false);
			break;
		case false:
			gameObject.SetActive (true);
			break;
		}
	}
}
