﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeHeight : MonoBehaviour {

	public InputField iF;
	public Slider s;

	public void ChangeValue()
	{
		iF.text = s.value.ToString();
	}
}
