﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameSpectatorBlock : MonoBehaviour {

	public PlayerBehaviourHumanoid PBH;
	public BlockData BD;

	private int currentID;
	public Text text;

	void Start()
	{
		//Position, Rotation, Animation
		currentID = 1;

	}

	void Update()
	{
		if (PBH.selectedBlock != currentID) 
		{
			currentID = PBH.selectedBlock;
			setBlock ();
		}
	}

	void setBlock()
	{
		text.text = "Selected: " + BD.models [currentID];
	}
		
}
