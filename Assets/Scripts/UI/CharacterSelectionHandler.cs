﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CharacterSelectionHandler : MonoBehaviour
{
    public Dropdown characterDropdown;

    public GameObject characterDropdownGO;
    public GameObject characterSelectionButtonGO;

    public void CreateCharacterDropdown()
    {
		string directory = Path.Combine(Application.dataPath, Path.Combine("Resources", "Humanoid"));

        //Get a list of worlds
        DirectoryInfo dir = new DirectoryInfo(directory);
        FileInfo[] worldList = dir.GetFiles("*.*");


        //for every world, create button
        foreach (FileInfo f in worldList)
        {
            //Skip unwanted files
            if ((f.Extension != ".prefab"))
            {
                continue;
            }

            //Log worlds
            Debug.Log(f.Name);

            //Add worlds to dropdown (Option name is world name without the .txt)
            characterDropdown.options.Add(new Dropdown.OptionData(Path.GetFileNameWithoutExtension(Path.Combine(directory, f.Name))));
        }
        characterSelectionButtonGO.SetActive(false);
        characterDropdownGO.SetActive(true);

    }

    public void SetCharacter()
    {
        Debug.Log(characterDropdown.options[characterDropdown.value].text);
        PlayerPrefs.SetString("race", characterDropdown.options[characterDropdown.value].text);
    }
}
