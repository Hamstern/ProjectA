﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnClick : MonoBehaviour 
{
	private float rotSpeed;

	public Text text;
	public Slider slider;
	public BlockData BD;
	public BlockBehaviour BB;
	private int currentID;

	void Start()
	{
		rotSpeed = 15f;
		currentID = Mathf.FloorToInt(slider.value);

		gameObject.transform.localScale = new Vector3(15f, 15f, 15f);
		gameObject.transform.localPosition = new Vector3 (165f, 131f, -495f);
	}

	void Update()
	{
		transform.Rotate (Vector3.up, rotSpeed * Time.deltaTime);
		ChangeBlock ();
	}

	void ChangeBlock()
	{
		if(slider.maxValue != BD.models.Length - 1)
		{
			slider.maxValue = BD.models.Length - 1;
		}
		if(slider.value != currentID)
		{
			currentID = Mathf.FloorToInt(slider.value);
			gameObject.GetComponent<MeshFilter> ().sharedMesh = BD.GetMesh(currentID);
			gameObject.GetComponent<MeshRenderer> ().material = BD.GetMaterial(currentID);
			text.text = BD.models [currentID];
		}
	}
		
}
