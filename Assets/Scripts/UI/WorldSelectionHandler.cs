﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class WorldSelectionHandler : MonoBehaviour {

    public Dropdown worldDropdown;
    public GameObject worldSelectionButtonGO;
    public GameObject worldDropdownGO;
    public GameObject inputHandler;

    public void CreateWorldButtons()
    {
		string directory = Path.Combine(Application.persistentDataPath, "Saves");
		Debug.Log (directory);

        //Get a list of worlds
		string[] dir = Directory.GetDirectories (directory);

		Debug.Log (dir.Length);
        //for every world, create button
        foreach (string f in dir)
        {

            //Log worlds
            Debug.Log(f);

            //Add worlds to dropdown (Option name is world name without the .txt)
            worldDropdown.options.Add(new Dropdown.OptionData(Path.GetFileNameWithoutExtension(Path.Combine(directory, f))));
        }
        worldSelectionButtonGO.SetActive(false);
        worldDropdownGO.SetActive(true);
        
    }

    public void SetWorldName()
    {
        PlayerPrefs.SetString("worldName", worldDropdown.options[worldDropdown.value].text);
    }


}
