﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CreateTerrainConfig : MonoBehaviour {

	public InputField configIF;
	public InputField NameIF;
	public Text warningText;

	public void CreateConfig()
	{
		warningText.gameObject.SetActive(false);
		if (NameIF.text != "" && configIF.text != "" && !NameIF.text.Contains ("|") && !NameIF.text.Contains (":")) {
			string dir = Path.Combine (Application.dataPath, Path.Combine ("Terrains", NameIF.text + ".txt"));
			File.WriteAllText (dir, configIF.text);
		} else {
			warningText.gameObject.SetActive(true);
		}
	}
}
