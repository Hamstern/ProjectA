﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class ImageScanner : MonoBehaviour {

	public Dropdown imgD;

	// Use this for initialization
	void Start () 
	{
		UpdateImages ();

	}

	public void UpdateImages()
	{
		imgD.ClearOptions ();
		string dir = Path.Combine(Application.dataPath, "Images");
		DirectoryInfo dirI = new DirectoryInfo(dir);
		FileInfo[] images = dirI.GetFiles("*.*");
		foreach(FileInfo i in images)
		{
			if(i.FullName.Contains(".meta"))
			{
				continue;
			}
			imgD.options.Add(new Dropdown.OptionData(Path.GetFileNameWithoutExtension(Path.Combine(dir, i.FullName))));
		}
	}
}
