﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class MenuHandler : MonoBehaviour {

    public GameObject loadingPanel;
    public Slider loadingSlider;
    public Text loadingText;
    public InputField worldKey;
    public InputField worldName;
	public Text warningText;

    public void GenerateWorld(int scene)
    {
		if (PlayerPrefs.GetString ("worldName") != "") 
		{
			warningText.gameObject.SetActive (false);
			StartCoroutine (LoadSync (scene));
		} else {
			warningText.gameObject.SetActive (true);
		}
    }

    IEnumerator LoadSync(int scene)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);

        loadingPanel.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            loadingSlider.value = progress;
            loadingText.text = progress * 100 + "%";
            yield return null;
        }
    }

    public void SetWorldPreference()
    {
        PlayerPrefs.SetString("worldName", worldName.text);
        
		if (worldKey.text == "" || worldKey.text == null) {
			Debug.Log ("Creating a random key!!!!");
			PlayerPrefs.SetInt ("worldKey", Random.Range (1000000, 100000));
		} else {
			Debug.Log ("Loading a set key!!!!");
			PlayerPrefs.SetInt("worldKey", int.Parse(worldKey.text));
		}
    }

}
