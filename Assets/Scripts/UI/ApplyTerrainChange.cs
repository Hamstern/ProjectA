﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApplyTerrainChange : MonoBehaviour 
{
	public BlockData BD;
	public InputField configIF;
	public InputField heightIF;
	public Slider IDS;

	public void UpdateInput()
	{
		if (IDS.value != 0 && heightIF.text != "") 
		{
			configIF.text += Mathf.Floor (IDS.value) + ":" + heightIF.text + "|";
		} 
	}
}
