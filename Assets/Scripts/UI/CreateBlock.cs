﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class CreateBlock : MonoBehaviour {


	public MeshSerializer MS;
	public BlockData BD;
	public GameObject GO;
	public InputField IF;
	public SpectateBlockAI SBAI;
	public Text warningText;

	public void CreateBlockFiles()
	{
		if (IF.text != "" || IF.text.Contains("|")) {
			warningText.gameObject.SetActive(false);
			SaveMaterials ();
			SaveMeshes ();
			EditConfig ();
		} else {
			warningText.gameObject.SetActive(true);
		}
	}

	private void SaveMaterials()
	{
		string dir = Path.Combine ("Assets", Path.Combine("Resources", Path.Combine("Materials", IF.text + ".txt")));
		MeshRenderer MR = GO.GetComponent<MeshRenderer> ();
		string materialData = MR.material.color.r + "|" + MR.material.color.g + "|" + MR.material.color.b + "|" + SBAI.imgURL + "|" + MR.material.mainTextureOffset.x + "|" + MR.material.mainTextureOffset.y + "|" + SBAI.isURL; //Red|Green|Blue|name|offset.x|offset.y|isURL(bool)
		File.WriteAllText(dir, materialData);
	}
	private void SaveMeshes()
	{
		string dir = Path.Combine("Assets", Path.Combine("Resources",Path.Combine("Block",IF.text + ".prefab")));
		byte[] data = MS.WriteMesh(GO.GetComponent<MeshFilter>().mesh, true);
		File.WriteAllBytes (dir, data);
	}
	private void EditConfig()
	{
		string configName = "IDconfig.txt";
		string dir = Path.Combine(Path.Combine(Application.dataPath, "Config"), configName);
		//Read a file and put the information to a string
		string tempString = File.ReadAllText(dir);

		if(!tempString.Contains("|" + IF.text + ":"))
		{
			File.WriteAllText(dir, tempString + "|" + IF.text + ":" + BD.models.Length);
		}
			
	}
}
