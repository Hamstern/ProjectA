﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class BlockData : MonoBehaviour
{

	public MeshSerializer MS;
	DirectoryInfo dir;
	public string[] models;
	Mesh[] meshes;
	Material[] materials;
	private string configName;
	public GameObject objectStructure;

	private void Start()
	{
		Debug.Log ("Loaded Blocks:");
		LoadConfig ();
		foreach(string s in models)
		{
			Debug.Log (s);
		}
	}
	public void LoadConfig() //Load ID config
	{
		configName = "IDconfig.txt";
		//Read a file and put the information to a string
		string tempString = File.ReadAllText(Path.Combine(Path.Combine(Application.dataPath, "Config"), configName));

		string[] tempArray = tempString.Split('|');//Looks like Name:ID\nName:ID\nName:ID... (\n is "a new array/line")

		string[] seperator;

		models = new string[tempArray.Length];
		meshes = new Mesh[tempArray.Length];
		materials = new Material[tempArray.Length];
		foreach(string data in tempArray)
		{
			if (data == "" || data == null) {
				continue;
			} else {
				seperator = data.Split (':');
				SetConfig (seperator);
				seperator = null;
			}

		}

	}

	private void SetConfig(string[] data)
	{
		models[int.Parse(data[1])] = data[0];
	}

	public Mesh GetMesh(int id)
	{
		string fileName = models[id];
		//Debug.Log ("Placing" + fileName);
		string dir = Path.Combine("Assets", Path.Combine("Resources",Path.Combine("Block",fileName + ".prefab")));
	//	Debug.Log ("Object in: " + models[id]);
		if (meshes [id] == null) 
		{
			meshes [id] = GetCacheItem(dir);
			if(meshes[id] == null)
			{
				meshes[id] = meshes[1];
			}

		}
	//	Debug.Log ("Mesh out: " + meshes[id]);
		return meshes [id];
	}

	private Mesh GetCacheItem(string dir)
	{
		if(File.Exists(dir) == true)
		{
			byte [] data = File.ReadAllBytes(dir);
			Mesh tempMesh = MS.ReadMesh (data);
			return tempMesh;
		}
		Debug.Log ("A Faulty Mesh");
		return meshes[1];
	}


	public Material GetMaterial(int id)
	{
		string fileName = models[id];
	//	Debug.Log ("Object in: " + models[id]);
		if (materials [id] == null) 
		{
			string materialDir = Path.Combine ("Assets", Path.Combine("Resources", Path.Combine("Materials", fileName + ".txt")));
			string materialText = File.ReadAllText (materialDir);
			string[] materialData = materialText.Split ('|');

			Material mat = new Material (Shader.Find("Diffuse"));

			if(materialData[3] != "")
			{
				Debug.Log (materialData[6]);
				if(materialData[6] == "True") // If it's a URL
				{
					WWW link = new WWW (materialData[3]);
					Debug.Log ("Downloading image " + link.texture.name);
					while(!link.isDone)
					{
						Debug.Log ("Downloading from url...");
					}
					mat.mainTexture = link.texture;
				}else{ // If it's on the disk
					Debug.Log ("Adding image " + materialData[3]);
					string linkURL = materialData[3];
					mat.mainTexture = Resources.Load (Path.Combine(Application.dataPath,Path.Combine("Images", linkURL)), typeof(Texture2D)) as Texture2D;
				}
				mat.mainTextureOffset = new Vector2 (float.Parse(materialData[4]),float.Parse(materialData[5]));
			}

			mat.color = new Color (float.Parse(materialData[0]),float.Parse(materialData[1]), float.Parse(materialData[2]), 1);
			materials [id] = mat;
			if(materials[id] == null)
			{
				materials[id] = materials[1];
			}

		}
	//	Debug.Log ("Texture out: " + materials[id]);
		return materials[id];
	}
}
	