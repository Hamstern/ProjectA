﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CreateBlockMenu : MonoBehaviour 
{

	public Slider objectSlider;
	public BlockBehaviour BB;
	public BlockData BD;
	public GameObject sphere;

	public void CreateRectMenu()
	{
		Debug.Log ("Called!!!!");
		int loop = 0;
		//for every terrain, create button
		foreach (string f in BD.models)
		{
			Debug.Log(loop + " and " + f);
			if (f == "Air") 
			{
				loop++;
				continue;
			}

			//Log terrains
			BB.CreateBlock (loop, new Vector3(600,loop *125, 0), sphere.transform);

			loop++;
		
		}
	}
}
