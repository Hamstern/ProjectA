﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldPreference : MonoBehaviour
{

    public WorldGenerationData WGD;
    public WorldGeneration WG;
    public Button SetKeyButton;
    public Slider KeySlider;
    public int tempKey;

    public void SetWorldKey()
    {
            WGD.key = tempKey;
            WGD.generated = false;
            WG.GenerateTerrain();
            Debug.Log("Setting world");
    }
    public void KeyChanged()
    {
        tempKey = (int) KeySlider.value;
        Debug.Log("Setting key");
    }
}
