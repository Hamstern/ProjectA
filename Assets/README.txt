Creating/Editing Terrains:
Go to directory Game\Assets\Terrains.
When adding or editing a terrain, create/open the .txt file.
You might see something like this.
2:5|4:7|1:20|3:Else
the first number after every | is the blocks id. in this case 2, 4, 1 and 3.
The second number tells which block to use when generating the world.
The terrain generates from top to bottom.
The declared number refers to the block X number of blocks down from the heighest block placed by the game.
In the first case it will place 5 layers of blockid 2.
Else is every block that haven't been declared.
In the case used it will place blockid 3 from block 21 and down till the map ends.
